for i in ["a",'b','c']:
    try:
        print(i**2)
    except TypeError:
        print("Uh oh spaghetti oh...  We have a type error!")

x = 5
y = 0
try:
    z = x/y
except:
    print("DO YOUR DIVISION RIGHT")
finally:
    print('All done')

def ask():
    while True:
        try:
            num = int(input('Input a integer:  '))
        except:
            print('An error occurred! Please try again!')
        else:
            print(f"Thank you, your number squared is: {num ** 2}")
            break
ask()
