# TIC TAC TOE GAME ON PYTHON
#This is a two player game
#By: Jake Nguyen

import re
replay = True
while replay == True:
#Gets User Input
    def get_user_input(myinput, *expectedInput):
        stop = False
        while stop == False:
            myinput = str(myinput.lower())
            if len(expectedInput) == 2:
                if myinput == expectedInput[0]:
                    stop = True
                elif myinput == expectedInput[1]:
                    stop = True
                else:
                    myinput = input("Please give the valid information:  ")
        return myinput
#Clears the screen
    def clear_screen():
        print('\n' * 100)
#Starts the game, getting the names of the two players and what side they will be on
    def start():
        print("What are your names?")
        name1 = input("Player 1:  ")
        name2 = input("Player 2:  ")
        player1 = input(f"{name1}... choose your side!(x or o):   ")
        side1 = get_user_input(player1, "x", "o")
        if side1 == "x":
            print(f"{name2}, that means you will be o!")
            side2 = "o"
        else:
            print(f"{name2}, that means you will be x!")
            side2 = "x"
        return side1, side2, name1, name2

#Code for displaying the tic tac toe board
    def display_board(board):
        print(f"{board[7]}|{board[8]}|{board[9]}")
        print("-----")
        print(f"{board[4]}|{board[5]}|{board[6]}")
        print("-----")
        print(f"{board[1]}|{board[2]}|{board[3]}")
#Starts a turn for player 1
    def tic_tac_toe_turn_1(player1, board, win):
        stp = False
        while stp == False:
            pattern = '^[1-9]$'
            plot1 = input(f"{player1['Name']}! Where would you like to go?:  ")
            result = re.match(pattern, plot1)
            if result:
                plot1 = int(plot1)
                if board[plot1] == ' ':
                    board[plot1] = player1['Side']
                    stap = True
                    break
                else:
                    print("That spot has already been taken!")
                    continue
            else:
                print("Please give a number from 1-9.")
                continue
        display_board(board)
#Starts a turn for player 2
    def tic_tac_toe_turn_2(player2, board, win):
        stap = False
        while stap == False:
            pattern = '^[1-9]$'
            plot2 = input(f"{player2['Name']}! Where would you like to go?:  ")
            result = re.match(pattern, plot2)
            if result:
                plot2 = int(plot2)
                if board[plot2] == ' ':
                    board[plot2] = player2['Side']
                    stap = True
                    break
                else:
                    print("That spot has already been taken!")
                    continue
            else:
                print("Please give a number from 1-9.")
                continue
        display_board(board)
#Asks if you want to replay the game
    def replay_ask():
        global replay
        yesorno = input("Would you like to replay?:  ")
        yesorno = get_user_input(yesorno, "yes", "no")
        if yesorno == "yes":
            replay = True
        else:
            clear_screen()
            exit("Thanks for playing!")
#Checks for win and tie
    def win_check(board, player1, player2, win):
        if board[1] == 'x' and board[2] == 'x' and board[3] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[1] == 'o' and board[2] == 'o' and board[3] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[4] == 'x' and board[5] == 'x' and board[6] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[4] == 'o' and board[5] == 'o' and board[6] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[7] == 'x' and board[8] == 'x' and board[9] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[7] == 'o' and board[8] == 'o' and board[9] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[1] == 'x' and board[4] == 'x' and board[7] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[1] == 'o' and board[4] == 'o' and board[7] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[2] == 'x' and board[2] == '5' and board[3] == '8':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[8] == 'o' and board[2] == 'o' and board[5] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[6] == 'x' and board[9] == 'x' and board[3] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[6] == 'o' and board[9] == 'o' and board[3] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[1] == 'x' and board[5] == 'x' and board[9] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[1] == 'o' and board[5] == 'o' and board[9] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[7] == 'x' and board[5] == 'x' and board[3] == 'x':
            if player1['Side'] == 'x':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[7] == 'o' and board[5] == 'o' and board[3] == 'o':
            if player1['Side'] == 'o':
                print(f"{player1['Name']} wins!")
                win = True
                return win
            else:
                print(f"{player2['Name']} wins!")
                win = True
                return win
        elif board[1] != ' ' and board[2] != ' ' and board[3] != ' ' and board[4] != ' ' and board[5] != ' ' and board[6] != ' ' and board[7] != ' ' and board[8] != ' ' and board[9] != ' ':
            print("Its a tie!")
            win = "tie"
            return win
        else:
            return False
    
    clear_screen()
    win = False
    sidesAndNames = start()
    player1 = {'Side' : sidesAndNames[0], "Name" : sidesAndNames[2]}
    player2 = {'Side': sidesAndNames[1], "Name": sidesAndNames[3]}
    board = ["#",' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
    display_board(board)
    while win == False:
        tic_tac_toe_turn_1(player1, board, win)
        win = win_check(board, player1, player2, win)
        if win == True:
            break
        elif win == "tie":
            break
        tic_tac_toe_turn_2(player2, board, win)
        win = win_check(board, player1, player2, win)
        if win == "tie":
            break
    replay_ask()
