"""
BLACKJACK
"""
import re
import random
print("\n" * 100)

class Deck:
    def __init__(self):
        suites = ['spades', 'hearts', 'diamonds', 'clubs']
        my_list = []
        for suite in suites:
            for value in range(2,15):
                if value in [11,12,13,14]:
                    if value == 14:
                        value = 'ace'
                    elif value == 11:
                        value = 'jack'
                    elif value == 12:
                        value = 'queen'
                    else:
                        value = 'king'
                my_list.append(Card(suite, value))
        self.cards = my_list

    def shuffle(self):
        random.shuffle(self.cards)

    def __str__(self):
        for card in self.cards:
            print(f"Value:  {card.value}\nSuite:  {card.suite}")




class Card:
    def __init__(self, suite, value):
        self.suite = suite
        self.value = value

    def __str__(self):
        return f"a {self.value} of {self.suite}"


class Hand:
    def __init__(self, deck, name):
        self.name = name
        cards = [deck.cards.pop(), deck.cards.pop()]
        self.total = 0
        for card in cards:
            if card.value == 'ace':
                if 21 - abs(11 + self.total) < 21 - abs(1 + self.total):
                    card.value = 11
                else:
                    card.value = 1
            elif card.value in ['jack','queen','king']:
                self.total = 10 + self.total
                continue
            self.total = card.value + self.total

        self.hand = cards

    def __str__(self):

        for i in range(0 ,len(self.hand)):
            my_string = f"Card {i+1}:  {self.hand[i].value} of {self.hand[i].suite}"
            print(my_string)
        return f"The sum of your cards is: {self.total}"

    def add(self, card):
        print(f"{self.name} hit and got {card}")
        self.hand.append(card)
        cards = self.hand
        self.total = 0
        for card in cards:
            if card.value == 'ace':
                if self.closer_to_21((11 + self.total), (1 + self.total)) == 1:
                    card.value = 11
                elif self.closer_to_21((11 + self.total), (1 + self.total)) == 2:
                    card.value = 1
                elif self.total == 21:
                    card.value = 1
                else:
                    pass
            elif card.value in ['jack', 'queen', 'king']:
                self.total = 10 + self.total
                continue
            self.total = card.value + self.total

        self.hand = cards

    def away_from_21(self, sum):
        return abs(21 - sum)

    def closer_to_21(self, sum1, sum2):
        if sum1 > 21 and sum2 < 21:
            return 2
        elif sum2 > 21 and sum1 < 21:
            return 1
        elif sum1 > 21 and sum2 > 21:
            return "bust"
        if self.away_from_21(sum1) < self.away_from_21(sum2):
            return 1
        else:
            return 2

    def twenty_one(self, sum):
        return sum == 21


class Chips:
    def __init__(self, money=100):
        self.money = money
        self.betted = 0

    def bet(self):
        print(f"Balance:   ${self.money}")
        stop = False
        while stop is False:
            amount = get_whole_number('How much would you like to bet?:  ')
            if self.money - amount < 0:
                print("Sorry! You don't have enough chips for that!" )
                continue
            else:
                self.betted = 0
                self.betted += amount
                self.money -= amount
                break

    def gain(self, amount, betted, tie=False):
        if tie == False:
            self.money += amount
            print(f'You won ${betted}!')
            print(f"You have ${self.money}")
        elif tie == True:
            self.money += amount

def get_whole_number(question):
    pattern = "^\d+$"
    stop = False
    while stop is False:
        string = input(question)
        match = re.match(pattern, string)
#        print(match)
        if match:
            stop = True
        if stop == False:
            print("Please input a whole number.")
    return int(string)



def hit_or_stay():
#    print(int(string))
    stop = False
    while stop == False:
        hos = input("Would you like to hit or stay?:  ")
        if hos == 'hit':
            return 'hit'
        elif hos == 'stay':
            return 'stay'
        else:
            print("That is not a valid answer...")

def show_some(handclass):
    for i in range(0, len(handclass.hand)):
        if i == 0:
            print(f"Card 1: ??? of ???")
        else:
            ace = handclass.hand[i].value
            if ace == 11 or ace == 1:
                ace = "ace"
            my_string = f"Card {i + 1}:  {ace} of {handclass.hand[i].suite}"
            print(my_string)


def show_all(handclass, identity):
    for i in range(0, len(handclass.hand)):
        ace = handclass.hand[i].value
        if ace == 11 or ace == 1:
            ace = "ace"
        my_string = f"Card {i + 1}:  {ace} of {handclass.hand[i].suite}"
        print(my_string)
    return f"The sum of {identity} cards is: {handclass.total}"

def player_busts(sum, player_hand):
    if sum > 21:
        for i in player_hand.hand:
            if i.value == 11 and sum - 10 < 21:
                i.value = 1
                sum -= 10
                return False
        print(player_hand)
        print(f"You bust! Your total was {sum}")
        return True
    else:
        return False


def win_check(playerhand, dealerhand, chips):
    player_win_sum = playerhand.away_from_21(player_hand.total)
    computer_win_sum = dealerhand.away_from_21(dealerhand.total)
    if player_win_sum < computer_win_sum:
        print("You Win!")
        chips.gain(chips.betted * 2, chips.betted)
    elif player_win_sum > computer_win_sum:
        print("You lose!")
        print(f"You lost ${chips.betted}!")
        print(f"You have ${chips.money}")
    elif player_win_sum == computer_win_sum:
        print("It's a tie!")
        print("No money was lost or gained.")
        chips.gain(chips.betted, chips.betted, True)


def dealer_wins():
    pass


def dealer_busts(sum):
    if sum > 21:
        return True
    else:
        return False

def replay_ask():
    stop = False
    while stop == False:
        replay_ask = input('Would you like to replay?:  ')
        if replay_ask == 'no':
            exit("Thanks For Playing!")
        elif replay_ask == 'yes':
            return True
        else:
            print('Please give a valid response. (yes or no)')


replay = True
my_chips = Chips(100000000000000000000000000)
skip_to_end = False
while replay is True:
    if skip_to_end == False:
        bust = False
        stay = False
        my_deck = Deck()
        my_deck.shuffle()
        cpu_hand = Hand(my_deck, "Dealer")
        player_hand = Hand(my_deck, "Player")
        player_gameplay = True
        print(f"{player_hand.name}'s Turn!")
        my_chips.bet()
        print("Dealer's Hand:")
        print(show_some(cpu_hand))
        while player_gameplay is True:
            print("Player's Hand:")
            print(show_all(player_hand, "your"))
            h_o_s = hit_or_stay()
            if h_o_s == 'hit':
                player_hand.add(my_deck.cards.pop())
                bust = player_busts(player_hand.total, player_hand)
                if bust is True:
                    print("You lose!")
                    print(f"You lost ${my_chips.betted}!")
                    print(f"You have ${my_chips.money}")
                    player_gameplay = False
                    skip_to_end = True
                    my_chips.betted = 0
            else:
                player_gameplay = False
                stay = True
        else:
            if stay is True:
                print("Dealer's Turn!")
                print(show_all(cpu_hand, "the dealer's"))
                while cpu_hand.away_from_21(cpu_hand.total) > player_hand.away_from_21(player_hand.total) or cpu_hand.away_from_21(cpu_hand.total) > 3:
                    cpu_hand.add(my_deck.cards.pop())
                    print(show_all(cpu_hand, "the dealer's"))
                    bust = dealer_busts(cpu_hand.total)
                    if bust is True:
                        print('Dealer busts and you win! You doubled your money!')
                        my_chips.gain(my_chips.betted * 2, my_chips.betted)
                        skip_to_end = True
                        break
                if bust is False:
                    win_check(player_hand,cpu_hand,my_chips)
                skip_to_end = True

    else:
        if my_chips.money == 0:
            exit("Uh Oh! It looks like you don't have any more money. Thanks for Playing!")
        replay = replay_ask()
        if replay is True:
            skip_to_end = False