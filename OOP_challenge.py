class Account():
    def __init__(self, owner, balance):
        self.owner = owner
        self.balance = balance
    def __str__(self):
        print_str = f"Account Owner:   {self.owner}\nAccount Balance:   {self.balance}"
        return print_str
    def deposit(self, deposit_num):
        self.balance = self.balance + deposit_num
        print("Deposit Accepted")
    def withdraw(self, withdraw_num):
        if self.balance - withdraw_num >= 0:
            self.balance = self.balance - withdraw_num
            print("Withdrawal Accepted")
        else:
            print("Funds Unavalible")
acct1 = Account('Jose', 100)
print(acct1)
print(acct1.owner)
print(acct1.balance)
acct1.deposit(50)
acct1.withdraw(75)
acct1.withdraw(999)