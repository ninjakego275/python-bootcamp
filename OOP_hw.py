import math
class Line():
    def __init__(self,c1,c2):
        self.c1 = c1
        self.c2 = c2
    def distance(self):
        dist = math.sqrt(((self.c2[0] - self.c1[0]) ** 2) + ((self.c2[1] - self.c1[1]) ** 2))
        return dist
    def slope(self):
        slp = (self.c2[1] - self.c1[1]) / (self.c2[0] - self.c1[0])
        return slp
coor1 = (3,2)
coor2 = (8,10)
li = Line(coor1,coor2)
print(li.distance())
print(li.slope())

class Cylinder():
    pi = math.pi
    def __init__(self, height, radius):
        self.height = height
        self.radius = radius
    def volume(self):
        vol =  self.pi * (self.radius ** 2) * self.height
        return vol
    def surface_area(self):
        surfaceArea = 2 * self.pi * self.radius * self.height + 2 * self.pi * (self.radius ** 2)
        return surfaceArea
c = Cylinder(2,3)
print(c.volume())
print(c.surface_area())
print(c.pi)