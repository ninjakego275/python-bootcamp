from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
driver = webdriver.Chrome('/Users/jakenguyen/Downloads/chromedriver')
driver.get('https://www.ixl.com/math/level-h/surface-area-of-cubes-and-rectangular-prisms')
user = "jnguyen15@norwoodcreek"
password = "camp55"
user_box = driver.find_elements_by_class_name('quick-login-text-input')[0]
user_box.send_keys(user)
pass_box = driver.find_elements_by_class_name("quick-login-text-input")[1]
pass_box.send_keys(password)
time.sleep(2)
sign_in_button = driver.find_elements_by_xpath('/html/body/div[8]/div/div[1]/div[2]/form/div/button')[0]
sign_in_button.click()
time.sleep(2)
driver.get('https://www.ixl.com/math/level-h/surface-area-of-cubes-and-rectangular-prisms')
time.sleep(1)
while True:
    ixl_element1 = driver.find_elements_by_xpath('/html/body/div[8]/section/div[1]/div[1]/div[3]/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/div[3]/table/tbody/tr/td/div')[0]
    ixlnum1 = ixl_element1.text
    index = 0
    for i in ixlnum1:
        if i == " ":
            splitter = index
        index += 1
    ixlnum1 = ixlnum1[:splitter]
    ixl_element2 = driver.find_elements_by_xpath('/html/body/div[8]/section/div[1]/div[1]/div[3]/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div')[0]
    ixlnum2 = ixl_element2.text
    index = 0
    for i in ixlnum2:
        if i == " ":
            splitter = index
        index += 1
    ixlnum2 = ixlnum2[:splitter]
    ixl_element3 = driver.find_elements_by_xpath('/html/body/div[8]/section/div[1]/div[1]/div[3]/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/div[1]/table/tbody/tr/td/div')[0]
    ixlnum3 = ixl_element3.text
    index = 0
    for i in ixlnum3:
        if i == " ":
            splitter = index
        index += 1
    ixlnum3 = ixlnum3[:splitter]
    text_area = driver.find_element_by_class_name('fillIn')
    answer = ((int(ixlnum3) * int(ixlnum2)) + (int(ixlnum1) * int(ixlnum3)) + (int(ixlnum1) * int(ixlnum2))) * 2
    text_area.send_keys(str(answer))
    #text_area = driver.find_element_by_xpath('/html/body/div[9]/section/div[1]/div[1]/div[3]/div/div[1]/div/div[1]/div/div/div/div/div[2]/span/span/input')
    #'<input type="text" class="fillIn" spellcheck="false" style="width: 58px;" id="yui_3_18_1_1_1590526556799_1286">'
    time.sleep(1)
    text_area.send_keys(Keys.RETURN)
    time.sleep(2)