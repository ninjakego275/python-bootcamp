import math
import numpy as np
import random
def vol(rad):
    v = 4/3 * (math.pi * (rad ** 3))
    v = round(v, 2)
    return v
print(vol(1))

def ran_check(num, low, high):
    if num >= low and num <= high:
        stri = f"{num} is in the range of {low} to {high}."
    else:
        stri = f"{num} is not in the range of {low} to {high}."
    print(stri)
    return num >= low and num <= high
print(ran_check(1, 2, 10))

def up_low(s):
    mystr = s.split()
    upper = 0
    lower = 0
    for string in mystr:
        for letter in string:
            if letter.isupper():
                upper += 1
            elif letter.islower():
                lower += 1
    return f"Number of Upper case characters: {upper}\nNumber of Lower case characters: {lower}"
print(up_low("Hello Mr. Rogers, how are you this fine Tuesday?"))

def unique_list(l):
    return set(l)

print(unique_list([1,1,1,1,1,2,2,2,3,4,4,4,4,4,4,4]))

def multiply(*numbers):
    product = 1
    for i in numbers:
        product *= i
    return product

print(multiply(1,2,3,-4))

def palindrome(word):
    backwards = word[::-1]
    return backwards == word

print(palindrome("helle"))

import string

def ispangram(str1, alphabet=string.ascii_lowercase):
    alphabet = [f for f in alphabet for e in f]
    str1 = str1.lower()
    for i in range(len(str1)):
        letter = str1[i]
        if letter in alphabet:
            index = alphabet.index(letter)
            alphabet.pop(index)
        print(alphabet)
    return alphabet == []
print(ispangram("The quick brown fox jumps over the lazy dog"))
def sort_nums(lst):
    lst.sort()
    return lst

def IQR(data):
    if len(data) % 2 == 1:
        firstHalf = int((len(data) / 2) - 0.5)
        secondHalf = int((len(data) / 2) + 0.5)
    elif len(data) % 2 == 0:
        firstHalf = int(len(data) / 2)
        secondHalf = int(len(data) / 2)
    lowerRange = data[:firstHalf]
    upperRange = data[secondHalf:]
    Q1 = np.median(lowerRange)
    Q3 = np.median(upperRange)
    IQR = Q3 - Q1
    median = np.median(data)
    print(data)
    print(f"The lower range is:  {lowerRange}")
    print(f"The upper range is:   {upperRange}")
    print(f"The first quartile is :  {Q1} ending at {firstHalf}")
    print(f"The median is:  {median}")
    print(f"The third quartile is: {Q3} starting at {secondHalf}")
    print(f"The IQR is:  {IQR}")

IQR(sort_nums([7,5,7,5,2,2,2,4,3]))
import matplotlib.pyplot as plt
import numpy
#fig, ax = plt.subplots()  # Create a figure containing a single axes.
#ax.plot([1, 2, 3, 4], [1, 4, 2, 3])
print((5 + 2/5) / (3+2/15))



def gensquares(n):
    for i in range(n):
        yield i**2
for x in gensquares(10):
    print(x)

def rand_num(low, high, n):
    for i in range(n):
        yield random.randint(low, high)
for num in rand_num(1, 10, 15):
    print(num)
s = 'hello'
s = iter(s)
print(s)
print(next(s))
print(next(s))

#You would want to use the yield generator statement in a case
#where you are looping though a large mass of data, and a generator
# would be much more efficient for memory because it is not storing a big list,
#it is just generating the numbers as it needss it.


#original_price = float(input("Original price:  "))
#sale = float(input("Sale:   "))
def calculate_sale_price(original, sale):
    discount = original * sale
    sale_price = original - discount
    print(sale_price)
#calculate_sale_price(original_price, sale)


